vcf-tools: a vCard file generator/swisstool
========================================

Features
---
* Three binaries (vcf-validate, vcf-info and vcf-merge)
* Check validity of .vcf files using vcf-validator 
* Parse vcf files 
* Parse abook files (through libabook if the feature is enabled)
* Export data to vcard format (.vcf file)
* Look for duplicate/existing entries 
* Automatically merge data/existing vcards
* Choose between different batch modes
* TODO: Interactively merge data/existing vcards
* TODO: ODF export

Installation
---

The default installation creates a single binary with subcommands:

``` $ cargo install --git https://gitlab.com/rogarb/vcf-tools ```

To install each command in separate binary files, enable the "multi" feature and
disable the default features:

``` $ cargo install --git https://gitlab.com/rogarb/vcf-tools --features=multi --no-default-features ```

Usage
---
```vcf-merge [-b mode] [-o output_vcf_file] [-s] file1 [file2 ...]```

`-b mode`: batch mode, where mode is
    copy (merges the input files without further processing them)
    unique_entries (keep only unique entries)
    duplicate_emails (attempt to merge entries containing the same emails)
    duplicate_phones (attempt to merge entries containing the same phones)
    duplicate_fields (attempt to merge entries containing the same value in any 
			field)
    
`-o output_file`: output to file (vcf format)

If no batch mode is specified, fallback to interactive mode (not yet 
implemented).
If no output file is specified, output is sent to STDOUT.
Format of input files is automatically detected by the program.


```vcf-validator [-V validator] [-q|-v] file1 [file2 ...]```

`-V validator`: which crate to use for the validation function, can be ical or
		vobject (default)
`-q`: quiet mode, no output is produced, result is sent using the exit code
`-v`: verbose, print extra information when non valid files
