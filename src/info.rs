use clap::Parser;

use libvcf::{stats::Stats, VCardProperty, VCF};
use std::path::PathBuf;
use strum::IntoEnumIterator;

/// Get information from vcf files.
///
/// This program is licensed under the GNU General Public License version 3 (GPLv3).
/// The source code of this program is published at <https://gitlab.com/rogarb/vcf-tools>
#[derive(Parser)]
#[clap(arg_required_else_help(true), author, version)]
pub struct VcfInfo {
    /// Input files
    #[clap(parse(from_os_str), required = true)]
    files: Vec<PathBuf>,
}

impl VcfInfo {
    pub fn run() -> Result<(), String> {
        VcfInfo::parse().process()
    }

    pub fn process(&self) -> Result<(), String> {
        for file in self.files.iter() {
            match VCF::try_from(file.as_path()) {
                Ok(vcf) => {
                    let title = format!("Summary for file {}", file.display());
                    let line = "-".repeat(title.len());
                    println!("{}\n{}\n{}", line, title, line);

                    println!("Number of entries: {}", vcf.len());

                    println!("Using built in counter");
                    for property in VCardProperty::iter() {
                        println!(
                            "\t- \"{}\" field: {} duplicates",
                            property,
                            vcf.count_property_duplicates(property)
                        );
                    }

                    println!("Using Stats:");
                    println!("{}", Stats::from(&vcf));
                }
                Err(e) => println!("Error: unable to parse file {}: {e}", file.display()),
            }
        }
        Ok(())
    }
}
