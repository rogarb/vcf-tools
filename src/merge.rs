use clap::Parser;
use libvcf::{format_detector, parser, stats::Stats, VCFBuilder, VCardProperty};
use std::ffi::OsStr;
use std::path::PathBuf;

/// Merge different contact files, optionally processing them, and output the result in vcard format
///
/// This program is licensed under the GNU General Public License version 3 (GPLv3).
/// The source code of this program is published at <https://gitlab.com/rogarb/vcf-tools>
/// Supported input formats: abook addressbook, VCard Format (.vcf file)
/// Supported batch modes: copy, unique_entries, duplicate_emails, duplicate_phones, duplicate_fields
#[derive(Parser)]
#[clap(
    name = "vcf-merge",
    author,
    verbatim_doc_comment,
    arg_required_else_help(true),
    version
)]
pub struct VcfMerge {
    /// Print stats
    ///
    /// Prints detailled statistics: processing time, number of initial entries, number of
    /// duplicates found and each sort, number of entries in output.
    /// The statistics are printed on stderr in order not to mess with the actual output.
    #[clap(short, long)]
    stats: bool,
    /// Batch process the merges input in the specified mode
    ///
    /// If provided, batch processes the merges input in the specified mode.
    /// If not provided, the program starts in interactive mode (not yet implemented).
    /// Supported batch modes:
    ///     - copy: no input processing
    ///     - unique_entries: filter unique entries
    ///     - duplicate_emails: merge entries possessing the same email value
    ///     - duplicate_phones: merge entries having the same phone value
    ///     - duplicate_fields: merge entries having the same value in all supported fields
    #[clap(short, long, verbatim_doc_comment, name = "mode")]
    batch: Option<BatchMode>,
    /// Output to a file
    ///
    /// If provided, the output is written to the specified file name, which will be automatically appended the vcf extension if not present.
    /// If not provided, the output is written to stdout.
    #[clap(
        short,
        long,
        verbatim_doc_comment,
        name = "output_file",
        parse(from_os_str)
    )]
    output_file: Option<PathBuf>,
    /// Input files
    ///
    /// A list of one or more input files to be processed and merged.
    /// File format is automatically detected by the program.
    /// Supported input formats: abook addressbook, VCard Format (.vcf file)
    #[clap(parse(try_from_os_str = path_validator), verbatim_doc_comment, required = true, name = "input_files")]
    input_files: Vec<PathBuf>,
    /// Do not validate input (vcf files only)
    ///
    /// If provided, vcf files will not be checked for format correctness and rejected if not valid.
    #[clap(long)]
    loose: bool,
    /// Ignore errors when parsing input (vcf files only)
    ///
    /// If provided, parsing errors will be ignored, yielding possibly empty output.
    /// Useful when using the --loose switch.
    /// It is recommended to use this option together with --split.
    #[clap(long, verbatim_doc_comment)]
    ignore_errors: bool,
    /// Internally split the input data (vcf files only)
    ///
    /// When using input files containing more than one VCARD entry, internally split the data to
    /// parse each VCARD entry separately. This should lead to better acceptance of the input
    /// files: instead of fully discarding a file, only non valid entries are discarded.
    /// To be used with --ignore-errors.
    #[cfg(feature = "ical")]
    #[clap(long)]
    split: bool,
}

fn path_validator(path: &OsStr) -> Result<PathBuf, String> {
    let path = PathBuf::from(path);
    if !path.exists() {
        Err(format!("{}: path does not exist", path.display()))
    } else if format_detector::detect_file_format(&path) == format_detector::Format::Other {
        Err(format!("{}: unsupported format", path.display()))
    } else {
        Ok(path)
    }
}

impl VcfMerge {
    pub fn run() -> Result<(), String> {
        Self::parse().process()
    }

    pub fn process(&self) -> Result<(), String> {
        if !self.loose {
            let mut err = false;
            for file in self.input_files.iter() {
                if format_detector::detect_file_format(file) == format_detector::Format::Vcard
                    && !parser::ParserBuilder::new()
                        .add_filename(file.clone())
                        .build()
                        .is_valid()
                {
                    eprintln!("Error: {} is not a valid .vcf file", file.display());
                    err = true;
                }
            }
            if err {
                return Err(
                    "Found invalid input files, consider using the --loose switch".to_string(),
                );
            }
        }

        #[cfg(feature = "ical")]
        let split = self.split;
        #[cfg(not(feature = "ical"))]
        let split = false;

        let output = VCFBuilder::new()
            .from_files(self.input_files.clone())
            .split(split)
            .ignore_errors(self.ignore_errors)
            .try_build()?;

        let stats = if self.stats {
            Some(Stats::from(&output))
        } else {
            None
        };

        let output = match self.batch {
            Some(BatchMode::UniqueEntries) => output.get_unique_entries(),
            Some(BatchMode::DuplicateEmails) => output.merge_from_property(VCardProperty::Email),
            Some(BatchMode::DuplicatePhones) => output.merge_from_property(VCardProperty::Phone),
            Some(BatchMode::DuplicateFields) => output.merge_all(),
            Some(BatchMode::Copy) => output,
            None => unimplemented!("Interactive mode is not yet implemented"), // Interactive mode
        };

        stats.iter().for_each(|s| {
            eprintln!("{s}");
            eprintln!("Changes:\n{}", s.diff_with(&output));
        });

        if let Some(file) = &self.output_file {
            output.write_to_file(file.to_path_buf())?;
        } else {
            println!("{}", output);
        }
        Ok(())
    }
}

use strum::IntoEnumIterator;
use strum_macros::{Display, EnumIter, EnumString};

/// Type representing the possible batch modes
#[derive(EnumIter, Debug, Display, EnumString)]
#[strum(serialize_all = "snake_case")]
pub enum BatchMode {
    /// Select only unique entries
    UniqueEntries,
    /// Merge entries containing duplicate email values
    DuplicateEmails,
    /// Merge entries containing duplicate phone values
    DuplicatePhones,
    /// Merge entries conaining any duplicate value in any field (except "VERSION" field)
    DuplicateFields,
    /// Don't touch the data, only copy
    Copy,
}

impl BatchMode {
    /// Export the available modes in a [`Vec<String>`].
    pub fn list_modes() -> Vec<String> {
        let mut modes = Vec::new();
        BatchMode::iter().for_each(|mode| modes.push(mode.to_string()));
        modes
    }
}
