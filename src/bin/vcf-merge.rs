use vcf_tools::merge::VcfMerge;

fn main() -> Result<(), String> {
    VcfMerge::run()
}
