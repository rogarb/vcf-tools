use vcf_tools::info::VcfInfo;

fn main() -> Result<(), String> {
    VcfInfo::run()
}
