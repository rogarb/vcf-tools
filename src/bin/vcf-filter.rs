use vcf_tools::filter::VcfFilter;

fn main() -> Result<(), String> {
    VcfFilter::run()
}
