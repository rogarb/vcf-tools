use vcf_tools::validator::VcfValidator;

fn main() -> Result<(), String> {
    VcfValidator::run()
}
