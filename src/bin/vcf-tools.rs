use vcf_tools::VcfTools;

fn main() -> Result<(), String> {
    VcfTools::run()
}
