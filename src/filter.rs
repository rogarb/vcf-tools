use clap::Parser;
use libvcf::{format_detector, VCFBuilder, VCardProperty, VCF};
use std::ffi::OsStr;
use std::path::PathBuf;
use strum::IntoEnumIterator;

/// Filter a .vcf file.
#[derive(Parser)]
#[clap(
    name = "vcf-filter",
    author,
    verbatim_doc_comment,
    arg_required_else_help(true),
    version
)]
pub struct VcfFilter {
    /// Output to a file
    ///
    /// If provided, the output is written to the specified file name, which will be automatically appended the vcf extension if not present.
    /// If not provided, the output is written to stdout.
    #[clap(
        short,
        long,
        verbatim_doc_comment,
        name = "output_file",
        parse(from_os_str)
    )]
    output_file: Option<PathBuf>,
    /// Input files
    ///
    /// A list of one or more input files to be processed and merged.
    /// File format is automatically detected by the program.
    /// Supported input formats: abook addressbook, VCard Format (.vcf file)
    #[clap(parse(try_from_os_str = path_validator), verbatim_doc_comment, required = true, name = "input_files")]
    input_files: Vec<PathBuf>,
    /// A substring to look for.
    #[clap(short, long, required = true)]
    substring_value: String,
    /// An optional list of properties to look into (by default, looks in all supported
    /// properties).
    ///
    /// Supported properties:
    ///  - name
    ///  - formatted-name
    ///  - email
    ///  - phone
    #[clap(verbatim_doc_comment, long, short)]
    properties: Option<Vec<VCardProperty>>,
}

fn path_validator(path: &OsStr) -> Result<PathBuf, String> {
    let path = PathBuf::from(path);
    if !path.exists() {
        Err(format!("{}: path does not exist", path.display()))
    } else if format_detector::detect_file_format(&path) == format_detector::Format::Other {
        Err(format!("{}: unsupported format", path.display()))
    } else {
        Ok(path)
    }
}

impl VcfFilter {
    pub fn run() -> Result<(), String> {
        Self::parse().process()
    }

    pub fn process(&self) -> Result<(), String> {
        let vcf = VCFBuilder::new()
            .from_files(self.input_files.clone())
            .try_build()?;
        let mut result = VCF::default();

        for property in self
            .properties
            .as_ref()
            .unwrap_or(&VCardProperty::iter().collect())
        {
            result.merge(vcf.filter_by_property_value(property, &self.substring_value));
        }
        let result = result.get_unique_entries();

        if let Some(file) = &self.output_file {
            result.write_to_file(file.to_path_buf())?;
        } else {
            println!("{}", result);
        }

        Ok(())
    }
}
