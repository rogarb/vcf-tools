use clap::{ArgGroup, Parser};
use libvcf::parser::{ParserBuilder, Parsers};
use std::{path::PathBuf, process};

/// Validate a list of vcf files
///
/// This program checks the given input files and tries parsing them. If parsing
/// is successful, the input file is reported as valid.
#[derive(Parser)]
#[clap(arg_required_else_help(true), group(ArgGroup::new("opt").multiple(false)), author, version)]
pub struct VcfValidator {
    /// .vcf files to check
    #[clap(parse(from_os_str), required = true)]
    vcf_files: Vec<PathBuf>,
    /// Validator selection
    #[clap(long)]
    validator: Option<Parsers>,
    /// Verbose output: get details about the non validity of the data
    #[clap(short, long, group = "opt")]
    verbose: bool,
    /// Quiet: get the result through exit code (0 is valid, 255 is not valid)
    #[clap(short, long, group = "opt")]
    quiet: bool,
}

impl VcfValidator {
    pub fn run() -> Result<(), String> {
        let validator = Self::parse();
        validator.process()
    }

    pub fn process(&self) -> Result<(), String> {
        if self.quiet {
            if self.vcf_files.len() != 1 {
                eprintln!("Only one file should be supplied in quiet mode");
                process::exit(1);
            }

            // cloning is necessary here to avoid the need of a &mut for self
            let filename = self.vcf_files[0].clone();

            if !filename.exists() {
                eprintln!("File doesn't exist: {}", filename.display());
                process::exit(1);
            }
            match ParserBuilder::new()
                .add_filename(filename)
                .parser_crate(self.validator)
                .build()
                .is_valid()
            {
                true => process::exit(0),
                false => process::exit(255),
            }
        } else if self.verbose {
            for filename in self.vcf_files.iter() {
                if !filename.exists() {
                    eprintln!("File doesn't exist: {}", filename.display());
                    continue;
                }
                match ParserBuilder::new()
                    .add_filename(filename.clone())
                    .parser_crate(self.validator)
                    .build()
                    .validate()
                {
                    Ok(_) => println!("{} is a valid .vcf file", filename.display()),
                    Err(e) => println!("{} is not a valid .vcf file:\n{}", filename.display(), e),
                }
            }
        } else {
            for filename in self.vcf_files.iter() {
                if !filename.exists() {
                    eprintln!("File doesn't exist: {}", filename.display());
                    continue;
                }
                match ParserBuilder::new()
                    .add_filename(filename.clone())
                    .parser_crate(self.validator)
                    .build()
                    .is_valid()
                {
                    true => println!("{} is a valid .vcf file", filename.display()),
                    false => println!("{} is not a valid .vcf file", filename.display()),
                }
            }
        }
        Ok(())
    }
}
