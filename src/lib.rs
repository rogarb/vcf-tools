use clap::{Parser, Subcommand};

pub mod filter;
pub mod info;
pub mod merge;
pub mod validator;

#[derive(Subcommand)]
#[clap(author, version)]
enum Command {
    Filter(filter::VcfFilter),
    Merge(merge::VcfMerge),
    Info(info::VcfInfo),
    Validate(validator::VcfValidator),
}

/// Swiss knife for vcf file manipulation.
#[derive(Parser)]
pub struct VcfTools {
    #[clap(subcommand)]
    command: Command,
}

impl VcfTools {
    pub fn run() -> Result<(), String> {
        let instance = Self::parse();
        match instance.command {
            Command::Filter(command) => command.process(),
            Command::Merge(command) => command.process(),
            Command::Info(command) => command.process(),
            Command::Validate(command) => command.process(),
        }
    }
}
